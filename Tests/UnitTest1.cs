using System;
using Xunit;
using System.Collections.Generic;
using CQRS2;

namespace Tests
{
    public class UnitTest1
    {
        //testy do command
        [Fact]
        //test dodawania do slownika
        public void Test1()
        {

            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            Assert.Equal(1, ser.amountToBuy);
        }
        //test usuwania 
        [Fact]
           public void Test2()
        {
                    
            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            var bought0 = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetAllItems());
             Assert.Equal(0, bought0.Count);
            event_broker.WriteModel(new  CQRS2.AddItemToCart("Ser", ser));
            var bought = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetAllItems());
            event_broker.WriteModel(new CQRS2.RemoveItemFromCart("Ser"));
            Assert.Equal(0, bought.Count);
            
        }
        //test zwiekszania ilosci rzeczy do kupienia
         [Fact]
        public void Test3()
        {
            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            Assert.Equal(1, ser.amountToBuy);
            Assert.Equal(0, ser.amountBought);
            event_broker.WriteModel(new CQRS2.IncreaseQuantityBought("Ser"));
            Assert.Equal(1, ser.amountBought);
            Assert.Equal(1, ser.amountToBuy);

        }
        //testy query:

        // wypisz wszystko
        [Fact]
        public void Test4()
        {
            
             CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            CQRS2.ShopItem bulka = new CQRS2.ShopItem("Biedronka", 1.99, 3);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            event_broker.WriteModel(new CQRS2.AddItemToCart("Bulka", bulka));
            var items = event_broker.ReadModel<Dictionary<string, ShopItem>>(new GetAllItems());

            Assert.Equal(2,items.Count);
        }
        // wypisz po nazwie produktu  
        [Fact]   
        public void Test5()
        {  
            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            CQRS2.ShopItem bulka = new CQRS2.ShopItem("Biedronka", 1.99, 3);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            event_broker.WriteModel(new CQRS2.AddItemToCart("Bulka", bulka));
            var items = event_broker.ReadModel<ShopItem>(new GetItemByName("Ser"));

            Assert.Equal(ser,items);
        }
        // wypisz kupione
        [Fact]
        public void Test6()
        {
            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            CQRS2.ShopItem bulka = new CQRS2.ShopItem("Biedronka", 1.99, 3);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            event_broker.WriteModel(new CQRS2.AddItemToCart("Bulka", bulka));
            var items = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetBoughtItems());

            Assert.Equal(0,items.Count);

            event_broker.WriteModel(new CQRS2.IncreaseQuantityBought("Ser"));
            var items2 = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetBoughtItems());

            Assert.Equal(1,items2.Count);
            event_broker.WriteModel(new CQRS2.IncreaseQuantityBought("Bulka"));

            var items3 = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetBoughtItems());

            Assert.Equal(1,items3.Count);


        }
        // wypisz rzeczy do kupienia
        [Fact]
        public void Test7()
        {
            CQRS2.EventBroker event_broker = new CQRS2.EventBroker();
            var cart = new CQRS2.ShoppingCart(event_broker);
            CQRS2.ShopItem ser = new CQRS2.ShopItem("Biedronka", 14.99, 1);
            CQRS2.ShopItem bulka = new CQRS2.ShopItem("Biedronka", 1.99, 3);
            event_broker.WriteModel(new CQRS2.AddItemToCart("Ser", ser));
            event_broker.WriteModel(new CQRS2.AddItemToCart("Bulka", bulka));
            var items = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetNotBoughtItems());

            Assert.Equal(2,items.Count);
            event_broker.WriteModel(new CQRS2.IncreaseQuantityBought("Ser"));
            var items2 = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetNotBoughtItems());

            Assert.Equal(1,items2.Count);

            event_broker.WriteModel(new CQRS2.IncreaseQuantityBought("Bulka"));
            var items3 = event_broker.ReadModel<Dictionary<string, ShopItem>>(new CQRS2.GetNotBoughtItems());

            Assert.Equal(1,items3.Count);

        }
    }
}
