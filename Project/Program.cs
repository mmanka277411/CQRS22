﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Collections.Generic;
using System.Linq;
namespace CQRS2
{
  public class ShoppingCart
    {
        public Dictionary<string, ShopItem> cart = new Dictionary<string, ShopItem>();
        EventBroker broker;


        public ShoppingCart(EventBroker broker)
        {
            this.broker = broker;
            this.broker.write += BrokerOnCommands;
            this.broker.read += BrokerOnQueries;
        }

        private void BrokerOnQueries(object sender, ReadModel e)
        {
            var query1 = e as GetAllItems;
            if (query1 != null)
            {
                query1.Result = cart;
                return;
            }

            var query2 = e as GetItemByName;
            if (query2 != null)
            {
                query2.Result = cart[query2.Target];
                return;
            }

            var query3 = e as GetBoughtItems;
            if (query3 != null)
            {
                query3.Result = cart.Where(pair => pair.Value.amountBought >= pair.Value.amountToBuy).ToDictionary(dict => dict.Key, dict => dict.Value);
                return;
            }

            var query4 = e as GetNotBoughtItems;
            if (query4 != null)
            {
                query4.Result = cart.Where(pair => pair.Value.amountBought < pair.Value.amountToBuy).ToDictionary(dict => dict.Key, dict => dict.Value);
                return;
            }

        }

        private void BrokerOnCommands(object sender, WriteModel e)
        {
            var command1 = e as AddItemToCart;
            if (command1 != null)
            {
                if (command1.Register)
                {
                    broker.allEvents.Add(new AddedItemToCartEvent(command1.name));
                }

                cart.Add(command1.name, command1.item);
                return;
            }

            var command2 = e as RemoveItemFromCart;
            if (command2 != null)
            {
                if (command2.Register)
                {
                    broker.allEvents.Add(new RemovedItemFromCartEvent(command2.name, cart[command2.name]));
                }

                cart.Remove(command2.name);
                return;
            }

            var command3 = e as IncreaseQuantityBought;
            if (command3 != null)
            {
                if (command3.Register)
                {
                    broker.allEvents.Add(new IncreasedQuantityOfItemEvent(command3.name));
                }

                cart[command3.name].amountBought++;
                return;
            }

            var command4 = e as DecreaseQuantityBought;
            if (command4 != null)
            {
                if (command4.Register)
                {
                    broker.allEvents.Add(new DecreasedQuantityOfItemEvent(command4.name));
                }

                cart[command4.name].amountBought++;
                return;
            }
            
        }
    }


    public class ShopItem
    {
        public string shopName;
        public double unitPrice;
        public int amountToBuy;
        public int amountBought;

        EventBroker broker;

        public ShopItem(string name, double price, int toBuy)
        {
            shopName = name;
            unitPrice = price;
            amountToBuy = toBuy;
            amountBought = 0;
        }

    }


    public class EventBroker
    {
        //wszystkie zdarzenia
        public IList<Event> allEvents = new List<Event>();


        // Commands
        public event EventHandler<WriteModel> write;

        //Query  
        public event EventHandler<ReadModel> read;
        public void WriteModel(WriteModel wm)
        {
            write?.Invoke(this, wm);
        }
        public T ReadModel<T>(ReadModel rm)
        {
            read?.Invoke(this, rm);
            return (T)rm.Result;
        }
        public void UndoLast()
        {
            var e = allEvents.LastOrDefault();

            var command1 = e as AddedItemToCartEvent;
            if(command1 != null)
            {
                WriteModel(new RemoveItemFromCart(command1.name) { Register = false });
                allEvents.Remove(e);
                return;
            }

            var command2 = e as RemovedItemFromCartEvent;
            if (command2 != null)
            {
                WriteModel(new AddItemToCart(command2.name, command2.item) { Register = false });
                allEvents.Remove(e);
                return;
            }

            var command3 = e as IncreasedQuantityOfItemEvent;
            if (command3 != null)
            {
                WriteModel(new DecreaseQuantityBought(command3.name) { Register = false });
                allEvents.Remove(e);
                return;
            }

            var command4 = e as DecreasedQuantityOfItemEvent;
            if (command4 != null)
            {
                WriteModel(new IncreaseQuantityBought(command4.name) { Register = false });
                allEvents.Remove(e);
                return;
            }
            

        }
    }

    public class ReadModel
    {
        public object Result;
    }
    public class GetAllItems : ReadModel
    {

    }
    public class GetItemByName : ReadModel
    {
        public string Target;
        public GetItemByName(string target)
        {
            Target = target;
        }
    }
   public class GetBoughtItems : ReadModel
    {

    }
    public class GetNotBoughtItems : ReadModel
    {

    }



    public class WriteModel : EventArgs
    {
        public bool Register = true;

    }
    public class AddItemToCart : WriteModel
    {
        public string name;
        public ShopItem item;

        public AddItemToCart(string name, ShopItem item)
        {
            this.name = name;
            this.item = item;
        }
    }
    public class RemoveItemFromCart : WriteModel
    {
        public string name;

        public RemoveItemFromCart(string name)
        {
            this.name = name;
        }
    }

    public class IncreaseQuantityBought : WriteModel
    {

        public string name;

        public IncreaseQuantityBought(string name)
        {
            this.name = name;
        }

    }
  public class DecreaseQuantityBought : WriteModel
    {
        public string name;

        public DecreaseQuantityBought(string name)
        {
            this.name = name;
        }
    }


    public class Event
    {

    }
   public class AddedItemToCartEvent : Event
    {
        public string name;

        public AddedItemToCartEvent(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return $"Added product \"{name}\" to cart";
        }
    }
       public class RemovedItemFromCartEvent : Event
    {
        public string name;
        public ShopItem item;

        public RemovedItemFromCartEvent(string name, ShopItem item)
        {
            this.name = name;
            this.item = item;
        }

        public override string ToString()
        {
            return $"Removed product \"{name}\" from cart";
        }

    }
       public class IncreasedQuantityOfItemEvent : Event
    {
        public string name;

        public IncreasedQuantityOfItemEvent(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return $"Increased quantity of \"{name}\" by one";
        }
    }

       public class DecreasedQuantityOfItemEvent : Event
    {
        public string name;

        public DecreasedQuantityOfItemEvent(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return $"Decreased quantity of \"{name}\" by one";
        }
    }
    
     class Program
    {
        static EventBroker event_broker;

        static void Main(string[] args)
        {

            event_broker = new EventBroker();
            var cart = new ShoppingCart(event_broker);

            ShopItem ser = new ShopItem("Biedronka", 14.99, 1);
            ShopItem mleko = new ShopItem("Biedronka", 3.19, 3);
            ShopItem zabawka = new ShopItem("Auchan", 49.99, 1);
//test w konsoli:
            event_broker.WriteModel(new AddItemToCart("Ser", ser));
            event_broker.WriteModel(new AddItemToCart("Mleko", mleko));
            event_broker.WriteModel(new AddItemToCart("Zabawka", zabawka));

            PrintAll();

            event_broker.WriteModel(new IncreaseQuantityBought("Ser"));
            event_broker.WriteModel(new IncreaseQuantityBought("Mleko"));
            event_broker.WriteModel(new IncreaseQuantityBought("Mleko"));

            PrintAll();

            event_broker.WriteModel(new RemoveItemFromCart("Zabawka"));

            PrintAll();

            event_broker.UndoLast();
            Console.WriteLine("------- UNDO -------");
            PrintAll();

            foreach (var e in event_broker.allEvents)
            {
                Console.WriteLine(e);
            }

            Console.ReadKey();
        }


        public static void PrintAll()
        {
            var items = event_broker.ReadModel<Dictionary<string, ShopItem>>(new GetAllItems());
            var bought = event_broker.ReadModel<Dictionary<string, ShopItem>>(new GetBoughtItems());
            var notbought = event_broker.ReadModel<Dictionary<string, ShopItem>>(new GetNotBoughtItems());

            Console.WriteLine("\nAll items:");
            foreach (var item in items)
            {
                Console.WriteLine(item.Key + " " + item.Value.amountBought + "/" + item.Value.amountToBuy);
            }
            Console.WriteLine("\nBought items:");
            foreach (var item in bought)
            {
                Console.WriteLine(item.Key + " " + item.Value.amountBought + "/" + item.Value.amountToBuy);
            }
            Console.WriteLine("\nNot bought items:");
            foreach (var item in notbought)
            {
                Console.WriteLine(item.Key + " " + item.Value.amountBought + "/" + item.Value.amountToBuy);
            }

            Console.WriteLine();
        }
    }
}
